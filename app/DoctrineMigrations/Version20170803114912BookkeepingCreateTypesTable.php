<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170803114912BookkeepingCreateTypesTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE bookkeeping.types (
                  id SERIAL NOT NULL,
                  name VARCHAR(20) NOT NULL,
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE UNIQUE INDEX types_name_unique_idx
                ON bookkeeping.types (lower(name))');

        $this->addSql("
            INSERT INTO bookkeeping.types (name) VALUES
                ('расход'),
                ('доход')
            ");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP TABLE bookkeeping.types;
        ');
    }
}
