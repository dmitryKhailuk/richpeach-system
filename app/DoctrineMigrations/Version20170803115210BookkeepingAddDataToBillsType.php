<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170803115210BookkeepingAddDataToBillsType extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
            INSERT INTO bookkeeping.bills_type (name, type_id) VALUES
                ('Зарплаты', 1),
                ('Реклама', 1),
                ('Офис', 1),
                ('Media-Buying', 2),
                ('Доход с трафбека', 2),
                ('Выплаты реклов', 2)
            ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        //not recent
    }
}
