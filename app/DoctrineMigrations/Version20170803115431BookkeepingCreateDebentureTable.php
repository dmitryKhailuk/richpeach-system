<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170803115431BookkeepingCreateDebentureTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE bookkeeping.debenture (
                  id SERIAL NOT NULL,
                  name VARCHAR(20) NOT NULL,
                  payment DECIMAL(15,2) DEFAULT 0,
                  date_payment DATE NOT NULL,
                  currency_id INTEGER NOT NULL,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE INDEX debenture_name_unique_idx
                ON bookkeeping.debenture (lower(name))');

        $this->addSql('
            ALTER TABLE bookkeeping.debenture ADD CONSTRAINT fk_debenture_currency_id_currencies_id 
                FOREIGN KEY (currency_id)
                REFERENCES bookkeeping.currency (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE bookkeeping.debenture
                DROP CONSTRAINT fk_debenture_currency_id_currency_id;
        ');

        $this->addSql('
            DROP TABLE bookkeeping.debenture;
        ');
    }
}
