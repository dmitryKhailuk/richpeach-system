<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170803115540BookkeepingCreatePaymentTypesTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE bookkeeping.payment_types (
                  id SERIAL NOT NULL,
                  name VARCHAR(30) NOT NULL,
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE INDEX payment_types_name_unique_idx
                ON bookkeeping.payment_types (lower(name))');

        $this->addSql("
            INSERT INTO bookkeeping.payment_types (name) VALUES
                ('WM'),
                ('WM без комиссии'),
                ('расчетный счет'),
                ('наличные'),
                ('карта')
            ");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP TABLE bookkeeping.payment_types;
        ');
    }
}
