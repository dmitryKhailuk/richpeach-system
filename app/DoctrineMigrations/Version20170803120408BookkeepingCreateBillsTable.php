<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170803120408BookkeepingCreateBillsTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE bookkeeping.bills (
                  id SERIAL NOT NULL,
                  name VARCHAR(100) NOT NULL,
                  payment DECIMAL(15,2) DEFAULT 0,
                  currency_id INTEGER NOT NULL,
                  type_id INTEGER NOT NULL,
                  bill_type_id INTEGER NOT NULL,
                  payment_type_id INTEGER NOT NULL,
                  comment TEXT DEFAULT NULL,
                  date_operation DATE DEFAULT NULL,
                  hashtag VARCHAR(250) DEFAULT NULL,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE INDEX bills_hashtag_lower_idx
                ON bookkeeping.bills (lower(hashtag))');

        $this->addSql('
            ALTER TABLE bookkeeping.bills ADD CONSTRAINT fk_bills_currency_id_currency_id 
                FOREIGN KEY (currency_id)
                REFERENCES bookkeeping.currency (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE bookkeeping.bills ADD CONSTRAINT fk_bills_type_id_types_id 
                FOREIGN KEY (type_id)
                REFERENCES bookkeeping.types (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE bookkeeping.bills ADD CONSTRAINT fk_bills_bill_type_id_bills_type_id 
                FOREIGN KEY (bill_type_id)
                REFERENCES bookkeeping.bills_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE bookkeeping.bills ADD CONSTRAINT fk_bills_payment_type_id_payment_types_id 
                FOREIGN KEY (payment_type_id)
                REFERENCES bookkeeping.payment_types (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP INDEX bookkeeping.bills_hashtag_lower_idx;;
        ');

        $this->addSql('
            ALTER TABLE bookkeeping.bills
                DROP CONSTRAINT fk_bills_payment_type_id_payment_types_id;
        ');

        $this->addSql('
            ALTER TABLE bookkeeping.bills
                DROP CONSTRAINT fk_bills_bill_type_id_bills_type_id;
        ');

        $this->addSql('
            ALTER TABLE bookkeeping.bills
                DROP CONSTRAINT fk_bills_type_id_types_id;
        ');

        $this->addSql('
            ALTER TABLE bookkeeping.bills
                DROP CONSTRAINT fk_bills_currency_id_currency_id;
        ');

        $this->addSql('
            DROP TABLE bookkeeping.bills;
        ');
    }
}
