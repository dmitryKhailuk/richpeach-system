<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170803135900TeamAddColumnEmployeeIdToDepartments extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

        $this->addSql('
            ALTER TABLE team.departments 
              ADD COLUMN employee_id INTEGER 
              REFERENCES team.employees (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE team.departments DROP COLUMN employee_id;
        ');

    }
}
