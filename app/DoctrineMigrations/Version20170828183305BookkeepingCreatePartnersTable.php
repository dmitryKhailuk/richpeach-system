<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170828183305BookkeepingCreatePartnersTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE bookkeeping.partners (
                  id SERIAL NOT NULL,
                  name VARCHAR(40) NOT NULL,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE INDEX partners_name_idx
                ON bookkeeping.partners (lower(name))');

        $this->addSql("
            INSERT INTO bookkeeping.partners (name) VALUES
                ('Даниил'),
                ('Антон'),
                ('Патч')
            ");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE bookkeeping.partners DROP INDEX partners_name_idx;
        ');

        $this->addSql('
            DROP TABLE bookkeeping.partners;
        ');

    }
}
