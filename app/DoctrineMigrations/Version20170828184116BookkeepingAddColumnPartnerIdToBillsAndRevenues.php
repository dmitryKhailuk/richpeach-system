<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170828184116BookkeepingAddColumnPartnerIdToBillsAndRevenues extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

        $this->addSql('
            ALTER TABLE bookkeeping.bills 
                ADD COLUMN partner_id INTEGER 
                REFERENCES bookkeeping.partners (id)
        ');

        $this->addSql('
            ALTER TABLE bookkeeping.revenues 
                ADD COLUMN partner_id INTEGER 
                REFERENCES bookkeeping.partners (id)
        ');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE bookkeeping.revenues DROP COLUMN partner_id;
        ');

        $this->addSql('
            ALTER TABLE bookkeeping.bills DROP COLUMN partner_id;
        ');

    }
}
