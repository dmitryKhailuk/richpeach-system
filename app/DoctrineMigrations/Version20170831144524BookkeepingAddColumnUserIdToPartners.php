<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170831144524BookkeepingAddColumnUserIdToPartners extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE bookkeeping.partners 
                ADD COLUMN user_id INTEGER 
                REFERENCES auth.user (id);
        ');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE bookkeeping.partners DROP COLUMN user_id;
        ');

    }
}
