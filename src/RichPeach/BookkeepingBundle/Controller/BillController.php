<?php

namespace RichPeach\BookkeepingBundle\Controller;

use Doctrine\ORM\QueryBuilder;
use RichPeach\BookkeepingBundle\Controller\Traits\BillsByThisYearTrait;
use RichPeach\BookkeepingBundle\Controller\Traits\GettingTotalSumCostTrait;
use RichPeach\BookkeepingBundle\Controller\Traits\GettingWebMoneyPaymentTrait;
use RichPeach\BookkeepingBundle\Entity\Bill;
use RichPeach\BookkeepingBundle\Entity\PaymentType;
use RichPeach\BookkeepingBundle\Entity\Type;
use RichPeach\BookkeepingBundle\Form\BillFilterType;
use RichPeach\BookkeepingBundle\Form\BillStatsFilterType;
use RichPeach\BookkeepingBundle\Form\BillType;
use RichPeach\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/bills")
 * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING')")
 */
class BillController extends Controller
{

    use ProcessesEntityRemovalTrait,  BillsByThisYearTrait, GettingWebMoneyPaymentTrait, GettingTotalSumCostTrait;

    /**
     * @Route("/{page}", name="bill_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @param int $page
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction(Request $request, $page)
    {
        $billRepository = $this->getDoctrine()->getRepository('RichPeachBookkeepingBundle:Bill');
        $rates = $request->getSession()->get('rates');

        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $billRepository->getListQueryBuilderCost();

        $form = $this->createForm(BillFilterType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdater $filterBuilder */
            $filterBuilder = $this->get('lexik_form_filter.query_builder_updater');
            $queryBuilder = $filterBuilder->addFilterConditions($form, $queryBuilder);
        }

        $bills = $queryBuilder->getQuery()->getArrayResult();
        list($sum, $totalSum) = $this->totalSumCost($queryBuilder, $this->container, $rates);

        if ($rates) {
            $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
            $bills = $converter->converterCurrency($bills, $request->getSession()->get('rates'));
        }

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $bills,
            $page,
            Bill::BILLS_PER_PAGE
        );

        return $this->render('@RichPeachBookkeeping/Bill/index.html.twig', [
            'pagination' => $pagination,
            'filter' => $form->createView(),
            'totalSum' => $totalSum,
            'sum' => $sum
        ]);
    }

    /**
     * @Route("/new", name="bill_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {

        $bill = new Bill();
        $form = $this->createForm(BillType::class, $bill);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $bill->setType($em->getRepository('RichPeachBookkeepingBundle:Type')->findOneBy(['id' => Type::LOSS ]));
            $paymentWithWebMoneyCommission = $this->getWebMoneyPayment($bill);
            if ($paymentWithWebMoneyCommission) {
                $bill->setPayment($paymentWithWebMoneyCommission);
            }
            $em->persist($bill);
            $em->flush();

            return $this->redirect($this->generateUrl('bill_index'));
        }

        return $this->render('@RichPeachBookkeeping/Bill/form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
        ]);
    }

    /**
     * @Route("/edit/{id}", name="bill_edit")
     * @Method({"GET", "PUT"})
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_SHOW_MODULE_PARTNER')")
     * @ParamConverter("bill", class="RichPeachBookkeepingBundle:Bill")
     * @param Request $request
     * @param Bill $bill
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, Bill $bill)
    {
        $partnerId = $request->query->get('partnerId');
        $form = $this->createForm(BillType::class, $bill, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $paymentWithWebMoneyCommission = $this->getWebMoneyPayment($bill);
            if ($paymentWithWebMoneyCommission) {
                $bill->setPayment($paymentWithWebMoneyCommission);
            }
            $em->persist($bill);
            $em->flush();

            $partnerId = $request->request->get('partnerId');
            if ($partnerId) {
                $partner = $em->getRepository('RichPeachBookkeepingBundle:Partner')->findOneBy(['id' => $partnerId ]);
                return $this->redirect($this->generateUrl('partner_index', ['id' => $partner->getId()]));
            }
            return $this->redirect($this->generateUrl('bill_index'));
        }

        return $this->render('@RichPeachBookkeeping/Bill/form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
            'partner' => $partnerId,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="bill_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_SHOW_MODULE_PARTNER')")
     * @ParamConverter("bill", class="RichPeachBookkeepingBundle:Bill")
     * @param Request $request
     * @param Bill $bill
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, Bill $bill)
    {
        $partnerId = $request->query->get('partnerId');
        $this->processEntityRemoval(
            $bill,
            $request,
            $this->container
        );
        if ($partnerId) {
            $em = $this->getDoctrine()->getManager();
            $partner = $em->getRepository('RichPeachBookkeepingBundle:Partner')->findOneBy(['id' => $partnerId ]);
            return $this->redirect($this->generateUrl('partner_index', ['id' => $partner->getId()]));
        }
        return $this->redirect($this->generateUrl('bill_index'));
    }

    /**
     * @Route("/bills_by_month", name="bill_bills_by_month")
     * @Method({"POST", "GET"})
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function billsByMonthAction(Request $request)
    {
        $dateTo = new \DateTime('now');
        $dateFrom = new \DateTime(date('Y') . BillStatsFilterType::FIRST_DATE_THIS_YEAR);
        $em = $this->getDoctrine()->getManager();
        $rates = $request->getSession()->get('rates');

        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $em->getRepository('RichPeachBookkeepingBundle:Bill')->getListQueryBuilderCost();

        $tableName = $em->getClassMetadata(Bill::class)->getSchemaName() . '.'
            . $em->getClassMetadata(Bill::class)->getTableName();

        $form = $this->createForm(BillStatsFilterType::class);
        $form->handleRequest($request);

        $billsByYear = $this->getBillsByThisYear($em, $tableName, $dateFrom, $dateTo);
        list($sum, $totalSum) = $this->totalSumCost($queryBuilder, $this->container, $rates);

        if ($form->isSubmitted() && $form->isValid()) {
            $dateFrom = $form->get('dateFrom')->getData();
            $dateTo = $form->get('dateTo')->getData();
            $billsByYear = $this->getBillsByThisYear($em, $tableName, $dateFrom, $dateTo);

            if ($form->get('currency')->getData()) {
                $converter = $this->get('RichPeach_bookkeeping_converter_currency_yahoo');
                $billsByYear = $converter->getConverterBillsByCurrency(
                    $form->get('currency')->getData(), $billsByYear);
            }
        }

        return $this->render('@RichPeachBookkeeping/Bill/billsByMonth.html.twig', [
            'billsByYear' => $billsByYear,
            'filter' => $form->createView(),
            'sum' => $sum,
            'totalSum' => $totalSum,
        ]);
    }

}
