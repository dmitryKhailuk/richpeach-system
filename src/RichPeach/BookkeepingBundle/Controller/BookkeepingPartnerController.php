<?php

namespace RichPeach\BookkeepingBundle\Controller;

use RichPeach\BookkeepingBundle\Entity\Partner;
use RichPeach\BookkeepingBundle\Form\PartnerType;
use RichPeach\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/partners")
 * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING')")
 */
class BookkeepingPartnerController extends Controller
{
    use ProcessesEntityRemovalTrait;

    /**
     * @Route("/{page}", name="bookkeeping_partner_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction($page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('RichPeachBookkeepingBundle:Partner')
            ->getListQueryBuilder();

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            Partner::PARTNERS_PER_PAGE
        );

        return $this->render('RichPeachBookkeepingBundle:Partner:index.html.twig', compact('pagination'));
    }

    /**
     * @Route("/new", name="bookkeeping_partner_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $partner = new Partner();
        $form = $this->createForm(PartnerType::class, $partner);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($partner);
            $em->flush();

            return $this->redirect($this->generateUrl('bookkeeping_partner_index'));
        }

        return $this->render('RichPeachBookkeepingBundle:Partner:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
        ]);
    }

    /**
     * @Route("/edit/{id}", name="bookkeeping_partner_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("partner", class="RichPeachBookkeepingBundle:Partner")
     * @param Request $request
     * @param Partner $partner
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, Partner $partner)
    {
        $form = $this->createForm(PartnerType::class, $partner, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($partner);
            $em->flush();

            return $this->redirect($this->generateUrl('bookkeeping_partner_index'));
        }

        return $this->render('RichPeachBookkeepingBundle:Partner:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
        ]);
    }

}
