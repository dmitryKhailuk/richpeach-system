<?php

namespace RichPeach\BookkeepingBundle\Controller;

use Doctrine\ORM\QueryBuilder;
use RichPeach\BookkeepingBundle\Controller\Traits\BillsByThisYearTrait;
use RichPeach\BookkeepingBundle\Controller\Traits\GettingTotalSumCostTrait;
use RichPeach\BookkeepingBundle\Controller\Traits\GettingTotalSumRevenueTrait;
use RichPeach\BookkeepingBundle\Controller\Traits\GettingWebMoneyPaymentTrait;
use RichPeach\BookkeepingBundle\Entity\Bill;
use RichPeach\BookkeepingBundle\Entity\Partner;
use RichPeach\BookkeepingBundle\Entity\Revenue;
use RichPeach\BookkeepingBundle\Entity\Type;
use RichPeach\BookkeepingBundle\Form\BillFilterType;
use RichPeach\BookkeepingBundle\Form\BillType;
use RichPeach\BookkeepingBundle\Form\RevenueType;
use RichPeach\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/partners")
 * @Security("has_role('ROLE_SHOW_MODULE_PARTNER') or has_role('ROLE_SHOW_MODULE_BOOKKEEPING')")
 */
class PartnerController extends Controller
{

    use ProcessesEntityRemovalTrait, BillsByThisYearTrait, GettingWebMoneyPaymentTrait, GettingTotalSumCostTrait;
    use GettingTotalSumRevenueTrait;
    /**
     * @Route("/bills/{id}/{page}", name="partner_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @ParamConverter("partner", class="RichPeachBookkeepingBundle:Partner")
     * @param int $page
     * @param Partner $partner
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction(Request $request, Partner $partner, $page)
    {
        $em = $this->getDoctrine()->getManager();
        $this->checkAccessToPage($partner);

        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $em->getRepository('RichPeachBookkeepingBundle:Bill')->getListQueryBuilderCost($partner);
        $rates = $request->getSession()->get('rates');

        $form = $this->createForm(BillFilterType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdater $filterBuilder */
            $filterBuilder = $this->get('lexik_form_filter.query_builder_updater');
            $queryBuilder = $filterBuilder->addFilterConditions($form, $queryBuilder);
        }

        $bills = $queryBuilder->getQuery()->getArrayResult();
        list($sum, $totalSum) = $this->totalSumCost($queryBuilder, $this->container, $rates);

        if ($rates) {
            $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
            $bills = $converter->converterCurrency($bills, $request->getSession()->get('rates'));
        }

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $bills,
            $page,
            Bill::BILLS_PER_PAGE
        );

        return $this->render('RichPeachBookkeepingBundle:Bill:index.html.twig', [
            'pagination' => $pagination,
            'filter' => $form->createView(),
            'totalSum' => $totalSum,
            'sum' => $sum,
            'partner' => $partner
        ]);
    }

    /**
     * @Route("/bills/{id}/new", name="partner_bill_new")
     * @Method({"GET", "POST"})
     * @ParamConverter("partner", class="RichPeachBookkeepingBundle:Partner")
     * @param Request $request
     * @param Partner $partner
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request, Partner $partner)
    {
        $this->checkAccessToPage($partner);

        $bill = new Bill();
        $form = $this->createForm(BillType::class, $bill);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $bill->setType($em->getRepository('RichPeachBookkeepingBundle:Type')->findOneBy(['id' => Type::LOSS ]));
            $bill->setPartner($partner);

            $paymentWithWebMoneyCommission = $this->getWebMoneyPayment($bill);
            if ($paymentWithWebMoneyCommission) {
                $bill->setPayment($paymentWithWebMoneyCommission);
            }
            $em->persist($bill);
            $em->flush();

            return $this->redirect($this->generateUrl('partner_index', ['id' => $partner->getId()]));
        }

        return $this->render('RichPeachBookkeepingBundle:Bill:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
            'partner' => $partner,
        ]);
    }

    /**
     * @Route("/revenue/{id}/{page}", name="partner_revenue_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @ParamConverter("partner", class="RichPeachBookkeepingBundle:Partner")
     * @param int $page
     * @param Partner $partner
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function revenueIndexAction(Request $request, Partner $partner, $page)
    {
        $this->checkAccessToPage($partner);

        $rates = $request->getSession()->get('rates');
        $em = $this->getDoctrine()->getManager();

        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $em->getRepository('RichPeachBookkeepingBundle:Revenue')->getListQueryBuilder($partner);

        $form = $this->createForm(BillFilterType::class, null, [
            BillFilterType::REVENUE_FILTER => true,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdater $filterBuilder */
            $filterBuilder = $this->get('lexik_form_filter.query_builder_updater');
            $queryBuilder = $filterBuilder->addFilterConditions($form, $queryBuilder);
        }

        $revenues = $queryBuilder->getQuery()->getArrayResult();

        list($sum, $totalSum) = $this->totalSumRevenue($queryBuilder, $this->container, $rates);

        if ($rates) {
            $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
            $revenues = $converter->converterCurrency($revenues, $rates);
        }

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $revenues,
            $page,
            Revenue::REVENUES_PER_PAGE
        );

        return $this->render('@RichPeachBookkeeping/Revenue/index.html.twig', [
            'pagination' => $pagination,
            'filter' => $form->createView(),
            'totalSum' => $totalSum,
            'sum' => $sum,
            'partner' => $partner
        ]);
    }

    /**
     * @Route("/revenue/{id}/new", name="partner_revenue_new")
     * @Method({"GET", "POST"})
     * @ParamConverter("partner", class="RichPeachBookkeepingBundle:Partner")
     * @param Request $request
     * @param Partner $partner
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newRevenueAction(Request $request, Partner $partner)
    {
        $this->checkAccessToPage($partner);

        $revenue = new Revenue();
        $form = $this->createForm(RevenueType::class, $revenue);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $paymentWithWebMoneyCommission = $this->getWebMoneyPayment($revenue);
            if ($paymentWithWebMoneyCommission) {
                $revenue->setPayment($paymentWithWebMoneyCommission);
            }
            $revenue->setPartner($partner);
            $em->persist($revenue);
            $em->flush();

            return $this->redirect($this->generateUrl('partner_revenue_index', ['id' => $partner->getId()]));
        }

        return $this->render('@RichPeachBookkeeping/Revenue/form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
            'partner' => $partner,
        ]);
    }

    /**
     * @param Partner $partner
     */
    private function checkAccessToPage(Partner $partner)
    {
        $securityBookkeeping = $this->get('security.authorization_checker')->isGranted('ROLE_SHOW_MODULE_BOOKKEEPING');
        $securityAdmin = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN');

        if (is_object($partner->getUser()) && $this->getUser()->getId() !== $partner->getUser()->getId() &&
            !$securityBookkeeping && !$securityAdmin) {
            throw new AccessDeniedHttpException();

        } else if (!is_object($partner->getUser())) {
            throw new NotFoundHttpException();
        }
    }
}
