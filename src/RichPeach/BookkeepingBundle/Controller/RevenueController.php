<?php

namespace RichPeach\BookkeepingBundle\Controller;

use RichPeach\BookkeepingBundle\Controller\Traits\BillsByThisYearTrait;
use RichPeach\BookkeepingBundle\Controller\Traits\GettingTotalSumRevenueTrait;
use RichPeach\BookkeepingBundle\Controller\Traits\GettingWebMoneyPaymentTrait;
use RichPeach\BookkeepingBundle\Entity\PaymentType;
use RichPeach\BookkeepingBundle\Entity\Revenue;
use RichPeach\BookkeepingBundle\Entity\Type;
use RichPeach\BookkeepingBundle\Form\BillFilterType;
use RichPeach\BookkeepingBundle\Form\BillStatsFilterType;
use RichPeach\BookkeepingBundle\Form\RevenueType;
use RichPeach\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/revenues")
 * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING')")
 */
class RevenueController extends Controller
{

    use ProcessesEntityRemovalTrait, BillsByThisYearTrait, GettingWebMoneyPaymentTrait, GettingTotalSumRevenueTrait;

    /**
     * @Route("/{page}", name="revenue_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @param int $page
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction(Request $request, $page)
    {
        $rates = $request->getSession()->get('rates');

        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('RichPeachBookkeepingBundle:Revenue')
            ->getListQueryBuilder();

        $form = $this->createForm(BillFilterType::class, null, [
            BillFilterType::REVENUE_FILTER => true,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdater $filterBuilder */
            $filterBuilder = $this->get('lexik_form_filter.query_builder_updater');
            $queryBuilder = $filterBuilder->addFilterConditions($form, $queryBuilder);
        }

        $revenues = $queryBuilder->getQuery()->getArrayResult();

        list($sum, $totalSum) = $this->totalSumRevenue($queryBuilder, $this->container, $rates);

        if ($rates) {
            $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
            $revenues = $converter->converterCurrency($revenues, $rates);
        }

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $revenues,
            $page,
            Revenue::REVENUES_PER_PAGE
        );

        return $this->render('@RichPeachBookkeeping/Revenue/index.html.twig', [
            'pagination' => $pagination,
            'filter' => $form->createView(),
            'totalSum' => $totalSum,
            'sum' => $sum
        ]);
    }

    /**
     * @Route("/new", name="revenue_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {

        $revenue = new Revenue();
        $form = $this->createForm(RevenueType::class, $revenue);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $paymentWithWebMoneyCommission = $this->getWebMoneyPayment($revenue);
            if ($paymentWithWebMoneyCommission) {
                $revenue->setPayment($paymentWithWebMoneyCommission);
            }

            $em->persist($revenue);
            $em->flush();

            return $this->redirect($this->generateUrl('revenue_index'));
        }

        return $this->render('@RichPeachBookkeeping/Revenue/form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
        ]);
    }

    /**
     * @Route("/edit/{id}", name="revenue_edit")
     * @Method({"GET", "PUT"})
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_SHOW_MODULE_PARTNER')")
     * @ParamConverter("revenue", class="RichPeachBookkeepingBundle:Revenue")
     * @param Request $request
     * @param Revenue $revenue
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, Revenue $revenue)
    {
        $partnerId = $request->query->get('partnerId');
        $form = $this->createForm(RevenueType::class, $revenue, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $paymentWithWebMoneyCommission = $this->getWebMoneyPayment($revenue);
            if ($paymentWithWebMoneyCommission) {
                $revenue->setPayment($paymentWithWebMoneyCommission);
            }
            $em->persist($revenue);
            $em->flush();
            $partnerId = $request->request->get('partnerId');
            if ($partnerId) {
                $partner = $em->getRepository('RichPeachBookkeepingBundle:Partner')->findOneBy(['id' => $partnerId ]);
                return $this->redirect($this->generateUrl('partner_revenue_index', ['id' => $partner->getId()]));
            }
            return $this->redirect($this->generateUrl('revenue_index'));
        }

        return $this->render('@RichPeachBookkeeping/Revenue/form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
            'partner' => $partnerId,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="revenue_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_SHOW_MODULE_PARTNER')")
     * @ParamConverter("revenue", class="RichPeachBookkeepingBundle:Revenue")
     * @param Request $request
     * @param Revenue $revenue
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, Revenue $revenue)
    {
        $partnerId = $request->query->get('partnerId');
        $this->processEntityRemoval(
            $revenue,
            $request,
            $this->container
        );
        if ($partnerId) {
            $em = $this->getDoctrine()->getManager();
            $partner = $em->getRepository('RichPeachBookkeepingBundle:Partner')->findOneBy(['id' => $partnerId ]);
            return $this->redirect($this->generateUrl('partner_index', ['id' => $partner->getId()]));
        }
        return $this->redirect($this->generateUrl('revenue_index'));
    }

    /**
     * @Route("/revenues_by_month", name="revenue_bills_by_month")
     * @Method({"POST", "GET"})
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function revenuesByMonthAction(Request $request)
    {
        $dateTo = new \DateTime('now');
        $dateFrom = new \DateTime(date('Y') . BillStatsFilterType::FIRST_DATE_THIS_YEAR);
        $rates = $request->getSession()->get('rates');
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $this->getDoctrine()->getRepository('RichPeachBookkeepingBundle:Revenue')
            ->getListQueryBuilder();

        $tableName = $em->getClassMetadata(Revenue::class)->getSchemaName() . '.'
            . $em->getClassMetadata(Revenue::class)->getTableName();

        $form = $this->createForm(BillStatsFilterType::class);
        $form->handleRequest($request);

        $billsByYear = $this->getBillsByThisYear($em, $tableName, $dateFrom, $dateTo);
        list($sum, $totalSum) = $this->totalSumRevenue($queryBuilder, $this->container, $rates);

        if ($form->isSubmitted() && $form->isValid()) {
            $dateFrom = $form->get('dateFrom')->getData();
            $dateTo = $form->get('dateTo')->getData();
            $billsByYear = $this->getBillsByThisYear($em, $tableName, $dateFrom, $dateTo);

            if ($form->get('currency')->getData()) {
                $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
                $billsByYear = $converter->getConverterBillsByCurrency(
                    $form->get('currency')->getData(), $billsByYear);
            }
        }

        return $this->render('@RichPeachBookkeeping/Bill/billsByMonth.html.twig', [
            'billsByYear' => $billsByYear,
            'filter' => $form->createView(),
            'type' => 'revenues',
            'sum' => $sum,
            'totalSum' => $totalSum,
        ]);
    }

}
