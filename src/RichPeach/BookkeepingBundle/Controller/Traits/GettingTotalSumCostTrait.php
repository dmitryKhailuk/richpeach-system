<?php

namespace RichPeach\BookkeepingBundle\Controller\Traits;

use Doctrine\ORM\QueryBuilder;

trait GettingTotalSumCostTrait
{

    /**
     * @param QueryBuilder $queryBuilder
     * @param $container
     * @param array $rates
     * @return array
     */
    public function totalSumCost(QueryBuilder $queryBuilder, $container, $rates = [])
    {
        $sum = 0;

        $totalSum = $queryBuilder->select('SUM(b.payment) as payment, c.name as currency, c.code')
            ->groupBy('c.code', 'c.name')
            ->orderBy('c.code', 'ASC')
            ->getQuery()->getArrayResult();

        if (is_array($rates) && count($rates) > 0) {
            $converter = $container->get('pinox_bookkeeping_converter_currency_yahoo');
            $total = $converter->converterCurrency($totalSum, $rates);
            foreach ($total as $item) {
                $sum += $item['payment'];
            }
        }

        return [$sum, $totalSum];
    }

}
