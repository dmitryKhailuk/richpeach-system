<?php

namespace RichPeach\BookkeepingBundle\Controller\Traits;

use Doctrine\ORM\QueryBuilder;
use RichPeach\BookkeepingBundle\Entity\Type;

trait GettingTotalSumRevenueTrait
{

    /**
     * @param QueryBuilder $queryBuilder
     * @param $container
     * @param array $rates
     * @return array
     */
    public function totalSumRevenue(QueryBuilder $queryBuilder, $container, $rates = [])
    {
        $sum = 0;

        $totalSum = $queryBuilder
            ->select('SUM(r.payment) as payment, c.name as currency, c.code, t.id as type')
            ->groupBy('c.code', 'c.name', 't.id')
            ->orderBy('c.code', 'ASC')
            ->getQuery()->getArrayResult();

        if (is_array($rates) && count($rates) > 0) {
            $converter = $container->get('pinox_bookkeeping_converter_currency_yahoo');
            $total = $converter->converterCurrency($totalSum, $rates);
            foreach ($total as $item) {
                if ($item['type'] === Type::INCOMING) {
                    $sum += $item['payment'];
                } else {
                    $sum -= $item['payment'];
                }

            }
        }
        foreach ($totalSum as $key => $item) {
            foreach ($totalSum as $keyTotal => $sumItem)
                if ($sumItem['type'] === Type::LOSS && $item['code'] === $sumItem['code'] && $key !== $keyTotal) {
                    $totalSum[$key]['payment'] -= $sumItem['payment'];
                    unset($totalSum[$keyTotal]);
                }
        }

        return [$sum, $totalSum];
    }

}
