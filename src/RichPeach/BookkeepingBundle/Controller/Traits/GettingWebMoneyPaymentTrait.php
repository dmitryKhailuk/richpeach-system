<?php

namespace RichPeach\BookkeepingBundle\Controller\Traits;


use RichPeach\BookkeepingBundle\Entity\PaymentType;

trait GettingWebMoneyPaymentTrait
{

    /**
     * @param $object
     * @return null|float
     */
    public function getWebMoneyPayment($object)
    {

        if (is_object($object->getPaymentType())
            && $object->getPaymentType()->getId() === PaymentType::PAYMENT_WEBMONEY) {
            return (float) $object->getPayment() + (($object->getPayment() * PaymentType::TAX_WEBMONEY)/100);
        }
        return null;
    }

}
