<?php
namespace RichPeach\BookkeepingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="BillRepository")
 * @ORM\Table(
 *     name="bills",
 *     schema="bookkeeping"
 * )
 */
class Bill
{
    use TimestampableEntity;

    const BILLS_PER_PAGE = 25;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="payment", type="decimal", precision=15, scale=2, nullable=false)
     * @Assert\GreaterThanOrEqual(value=0)
     * @var float
     */
    private $payment = 0;

    /**
     * @ORM\Column(name="comment", type="string", length=2000)
     * @Assert\Length(max=2000)
     */
    private $comment;

    /**
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="Currency",
     *     cascade={"persist"})
     * @Assert\NotBlank()
     * @var Currency
     */
    private $currency;

    /**
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=true)
     * @ORM\ManyToOne(targetEntity="Type",
     *     cascade={"persist"})
     * @var Type
     */
    private $type;

    /**
     * @ORM\JoinColumn(name="bill_type_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="BillType",
     *     cascade={"persist"})
     * @Assert\NotBlank()
     * @var BillType
     */
    private $billType;

    /**
     * @ORM\JoinColumn(name="payment_type_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="PaymentType",
     *     cascade={"persist"})
     * @Assert\NotBlank()
     * @var PaymentType
     */
    private $paymentType;

    /**
     * @ORM\Column(name="date_operation", type="date", nullable=true)
     * @Assert\Date()
     * @var \DateTime
     */
    private $dateOperation;

    /**
     * @ORM\Column(name="hashtag", type="string", length=250, nullable=true)
     * @Assert\Length(max="250")
     * @var string
     */
    private $hashtag;

    /**
     * @ORM\JoinColumn(name="partner_id", referencedColumnName="id", nullable=true)
     * @ORM\ManyToOne(targetEntity="Partner",
     *     cascade={"persist"})
     * @var Partner
     */
    private $partner;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Bill
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set payment
     *
     * @param string $payment
     *
     * @return Bill
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return string
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Bill
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set currency
     *
     * @param \RichPeach\BookkeepingBundle\Entity\Currency $currency
     *
     * @return Bill
     */
    public function setCurrency(Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \RichPeach\BookkeepingBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set type
     *
     * @param \RichPeach\BookkeepingBundle\Entity\Type $type
     *
     * @return Bill
     */
    public function setType(Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \RichPeach\BookkeepingBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set billType
     *
     * @param \RichPeach\BookkeepingBundle\Entity\BillType $billType
     *
     * @return Bill
     */
    public function setBillType(BillType $billType = null)
    {
        $this->billType = $billType;

        return $this;
    }

    /**
     * Get billType
     *
     * @return \RichPeach\BookkeepingBundle\Entity\BillType
     */
    public function getBillType()
    {
        return $this->billType;
    }

    /**
     * Set paymentType
     *
     * @param \RichPeach\BookkeepingBundle\Entity\PaymentType $paymentType
     *
     * @return Bill
     */
    public function setPaymentType(PaymentType $paymentType = null)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get paymentType
     *
     * @return \RichPeach\BookkeepingBundle\Entity\PaymentType
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * Set dateOperation
     *
     * @param \DateTime $dateOperation
     *
     * @return Bill
     */
    public function setDateOperation($dateOperation)
    {
        if ($dateOperation) {
            $this->dateOperation = $dateOperation;
        } else {
            $this->dateOperation = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get dateOperation
     *
     * @return \DateTime
     */
    public function getDateOperation()
    {
        return $this->dateOperation;
    }

    /**
     * Set hashtag
     *
     * @param string $hashtag
     *
     * @return Bill
     */
    public function setHashtag($hashtag)
    {
        $this->hashtag = $hashtag;

        return $this;
    }

    /**
     * Get hashtag
     *
     * @return string
     */
    public function getHashtag()
    {
        return $this->hashtag;
    }

    /**
     * Set partner
     *
     * @param \RichPeach\BookkeepingBundle\Entity\Partner $partner
     *
     * @return Bill
     */
    public function setPartner(Partner $partner)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * Get partner
     *
     * @return \RichPeach\BookkeepingBundle\Entity\Partner
     */
    public function getPartner()
    {
        return $this->partner;
    }
}
