<?php

namespace RichPeach\BookkeepingBundle\Entity;

use Doctrine\ORM\EntityRepository;

class BillTypeRepository extends EntityRepository
{
    /**
     * @param array $criteria
     * @return mixed
     */
    public function findInterferingBillsTypeByCriteria(array $criteria)
    {
        return $this->createQueryBuilder('bt')
            ->where('lower(bt.name) = lower(:name)')
            ->andWhere('bt.type = :type')
            ->setParameters([
                'name' => $criteria['name'],
                'type' => $criteria['type'],
            ])
            ->getQuery()
            ->execute();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('bt')
            ->join('bt.type', 't')
            ->orderBy('bt.name', 'ASC')
            ->addOrderBy('t.id', 'ASC');
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListCostQueryBuilder()
    {
        return $this->createQueryBuilder('bt')
            ->join('bt.type', 't')
            ->where('t.id = :cost')
            ->setParameter('cost', Type::LOSS)
            ->orderBy('bt.name', 'ASC');
    }


    /**
     * @param integer $typeId
     * @return BillType[]
     */
    public function findBillTypesByType($typeId)
    {
        return $this->createQueryBuilder('bt')
            ->join('bt.type', 't')
            ->where('t.id = :type')
            ->setParameter('type', $typeId)
            ->orderBy('bt.name', 'ASC')
            ->getQuery()->getArrayResult();
    }

}
