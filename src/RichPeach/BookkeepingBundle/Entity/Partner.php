<?php
namespace RichPeach\BookkeepingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RichPeach\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="PartnerRepository")
 * @ORM\Table(
 *     name="partners",
 *     schema="bookkeeping",
 *     indexes={
 *          @ORM\Index(name="partners_name_idx", columns={"name"})
 *      }
 * )
 */
class Partner
{
    use TimestampableEntity;

    const PARTNERS_PER_PAGE = 25;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=40, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="40")
     * @var string
     */
    private $name;

    /**
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     * @ORM\OneToOne(targetEntity="RichPeach\UserBundle\Entity\User",
     *     inversedBy="partner",
     *     cascade={"persist"})
     * @var User
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Partner
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set user
     *
     * @param \RichPeach\UserBundle\Entity\User $user
     *
     * @return Partner
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \RichPeach\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
