<?php

namespace RichPeach\BookkeepingBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PartnerRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.name', 'ASC');
    }
}
