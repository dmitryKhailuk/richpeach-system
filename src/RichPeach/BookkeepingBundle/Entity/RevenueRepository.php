<?php

namespace RichPeach\BookkeepingBundle\Entity;

use Doctrine\ORM\EntityRepository;

class RevenueRepository extends EntityRepository
{
    /**
     * @param Partner|null $partner
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder(Partner $partner = null)
    {
        $qb = $this->createQueryBuilder('r')
            ->select('r.id, r.name, r.payment, r.comment, r.createdAt, c.name AS currency, t.name AS type, c.code,
                bt.name AS billType, pt.name AS paymentType, r.dateOperation, r.hashtag')
            ->leftJoin('r.currency', 'c')
            ->leftJoin('r.billType', 'bt')
            ->leftJoin('r.type', 't')
            ->leftJoin('r.paymentType', 'pt')
            ->orderBy('r.createdAt', 'DESC');

        if ($partner) {
            $qb = $qb->leftJoin('r.partner', 'p')
                ->andWhere('p.id = :partner')
                ->setParameter('partner', $partner->getId());
        }
        return $qb;
    }

    /**
     * @return array
     */
    public function getTotalSumRevenuesFromCurrency()
    {
        return $this->createQueryBuilder('r')
            ->select('SUM(r.payment) as payment, c.name as currency, c.code, t.id as type')
            ->innerJoin('r.currency', 'c')
            ->innerJoin('r.type', 't')
            ->groupBy('c.code', 'c.name', 't.id')
            ->orderBy('c.code', 'ASC')
            ->getQuery()->getArrayResult();
    }
}
