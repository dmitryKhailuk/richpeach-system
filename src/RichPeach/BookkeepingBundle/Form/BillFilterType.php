<?php

namespace RichPeach\BookkeepingBundle\Form;


use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\DateRangeFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use RichPeach\BookkeepingBundle\Entity\BillTypeRepository;
use RichPeach\BookkeepingBundle\Entity\CurrencyRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BillFilterType extends AbstractType
{
    const REVENUE_FILTER = 'revenue_filter';
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $parameters = [
            'label'    => 'bill.bill_type',
            'class' => 'RichPeachBookkeepingBundle:BillType',
            'choice_label' => 'name',
            'required' => false,
            'label_attr' => [ 'class' => 'filter-label' ],
        ];

        if ($options[self::REVENUE_FILTER] === true) {
            $builder->add('type', EntityFilterType::class, [
                'label'    => 'bill.type',
                'class' => 'RichPeachBookkeepingBundle:Type',
                'choice_label' => 'name',
                'required' => false,
                'label_attr' => [ 'class' => 'filter-label' ],
            ]);

        } else {
            $parameters['query_builder'] = function (BillTypeRepository $repository) {
                return $repository->getListCostQueryBuilder();
            };
        }

        $builder->add('billType', EntityFilterType::class, $parameters);

        $builder->add('currency', EntityFilterType::class, [
            'label' => 'bill.currency',
            'class' => 'RichPeachBookkeepingBundle:Currency',
            'choice_label' => 'name',
            'required' => false,
            'query_builder' => function (CurrencyRepository $repository) {
                return $repository->getListQueryBuilder();
            },
            'label_attr' => [ 'class' => 'filter-label' ],
        ]);

        $builder->add('createdAt', DateRangeFilterType::class, [
            'required' => false,
            'label'    => false,
            'left_date_options' => [
                'required' => false,
                'html5' => false,
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
                'label' => 'bill.date_from',
                'label_attr' => [ 'class' => 'filter-label max-width' ],
                'attr' => [
                    'placeholder' => 'common.date_calendar',
                    'data-show-as' => 'datepicker',
                ]
            ],
            'right_date_options' => [
                'required' => false,
                'html5' => false,
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
                'label' => 'bill.date_to',
                'label_attr' => [ 'class' => 'filter-label max-width' ],
                'attr' => [
                    'placeholder' => 'common.date_calendar',
                    'data-show-as' => 'datepicker',
                ]
            ]

        ]);

        $builder->add('hashtag', TextFilterType::class, [
            'label'    => 'bill.hashtag',
            'required' => false,
            'label_attr' => [ 'class' => 'filter-label' ],
        ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'method'            => 'GET',
            self::REVENUE_FILTER => false,
        ]);

        $resolver->setAllowedValues(self::REVENUE_FILTER, [true, false]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'richpeach_bookkeeping_bills_filter';
    }
}