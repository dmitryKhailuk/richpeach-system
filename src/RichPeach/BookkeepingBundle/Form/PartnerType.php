<?php

namespace RichPeach\BookkeepingBundle\Form;

use RichPeach\BookkeepingBundle\Entity\Partner;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartnerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'partner.name',
            'required' => true,
            'attr' => [
                'maxlength' => 40
            ]
        ]);

        $builder->add('user', EntityType::class, [
            'label' => 'partner.user',
            'class' => 'RichPeachUserBundle:User',
            'choice_label' => 'username',
            'required' => false,
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Partner::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'richpeach_bookkeeping_partner';
    }
}