<?php

namespace RichPeach\DashboardBundle\Controller;

use RichPeach\BookkeepingBundle\Entity\Currency;
use RichPeach\DashboardBundle\Controller\Traits\GettingExchangeRatesAndAddToDb;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Session\Session;

class DashboardMainController extends Controller
{
    use GettingExchangeRatesAndAddToDb;

    /**
     * @Route("/", name="dashboard")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction()
    {
        $securityBookkeeping = $this->get('security.authorization_checker')->isGranted('ROLE_SHOW_MODULE_BOOKKEEPING');
        $securityAdmin = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN');
        $securityPartner = $this->get('security.authorization_checker')->isGranted('ROLE_SHOW_MODULE_PARTNER');
        $em = $this->getDoctrine()->getManager();
        $partners = [];

        if ($securityPartner && !$securityBookkeeping && !$securityAdmin) {
            $partners = $em->getRepository('RichPeachBookkeepingBundle:Partner')->findBy([
                'user' => $this->getUser()
            ]);

        } else if ($securityBookkeeping || $securityAdmin) {
            $partners = $em->getRepository('RichPeachBookkeepingBundle:Partner')->findAll();
        }

        $session = new Session();
        $session->set('partners', $partners);

        return $this->render('RichPeachDashboardBundle:DashboardMain:index.html.twig');
    }

}
