<?php

namespace RichPeach\DashboardBundle\Controller\Traits;

use Doctrine\ORM\EntityManager;
use RichPeach\BookkeepingBundle\Entity\Currency;
use RichPeach\BookkeepingBundle\Entity\ExchangeRate;

trait GettingExchangeRatesAndAddToDb
{
    /**
     * @param Currency $currency
     * @param EntityManager $em
     * @return array
     */
    public function gettingExchangeRatesAndAddToDb(Currency $currency, EntityManager $em)
    {
        $rates = [];
        $exchangeRates = $em->getRepository('RichPeachBookkeepingBundle:ExchangeRate')
            ->findExchangeRatesByCurrencyFoNowDate($currency->getId());

        if ($exchangeRates) {
            foreach ($exchangeRates as $rate) {
                $rates[$rate['code_rate']] = [
                    'rate' => $rate['rate'],
                    'codeRate' => $rate['code_rate'],
                    'currencyId' => $rate['currency_id'],
                ];
            }
        } else {
            $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
            $converterRates = $converter->getCurrencyRateXchange($currency->getCode());
            foreach ($converterRates as $converterRate) {
                $exchangeRate = new ExchangeRate();
                $exchangeRate->setCurrency($currency);
                $exchangeRate->setCodeRate($converterRate['id']);
                $exchangeRate->setRate($converterRate['Rate']);
                $rates[$converterRate['id']] = [
                    'rate' => $converterRate['Rate'],
                    'codeRate' => $converterRate['id'],
                    'currencyId' => $currency->getId(),
                ];
                $em->persist($exchangeRate);
            }

            $em->flush();
        }

        return $rates;
    }
}
