<?php

namespace RichPeach\TeamBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/dashboard")
 */
class DashboardController extends Controller
{
    /**
     * @Route("/view", name="dashboard_view")
     * @Security("has_role('ROLE_SHOW_MODULE_TEAM')")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction()
    {
        $departmentsTree = [];
        $tree = [];
        $em = $this->getDoctrine()->getManager();

        $departments = $em->getRepository('RichPeachTeamBundle:Department')->getDepartmentsForTree();

        foreach ($departments as $key => $department) {
            if (empty($department['parentId']) || $department['id'] === $department['parentId']) {
                $departments[$key]['parentId'] = 0;
            }
            $departmentsTree[$departments[$key]['parentId']][] = $departments[$key];
        }

        if (array_key_exists('0', $departmentsTree)) {
            $tree = $this->createTree($departmentsTree, $departmentsTree[0]);
        }

        return $this->render('@RichPeachTeam/Dashboard/index.html.twig', [
            'tree' => $tree
        ]);
    }

    /**
     * @param $parents
     * @param $children
     * @return array
     */
    private function createTree(&$parents, $children) {
        $tree = [];

        foreach ($children as $child){

            if(isset($parents[$child['id']])) {
                $child['children'] = $this->createTree($parents, $parents[$child['id']]);
            }

            $tree[] = $child;
        }
        return $tree;
    }
}