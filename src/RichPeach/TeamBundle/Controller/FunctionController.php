<?php

namespace RichPeach\TeamBundle\Controller;

use RichPeach\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use RichPeach\TeamBundle\Entity\FunctionTable;
use RichPeach\TeamBundle\Form\FunctionTableType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/functions")
 * @Security("has_role('ROLE_SHOW_MODULE_TEAM')")
 */
class FunctionController extends Controller
{
    use ProcessesEntityRemovalTrait;

    /**
     * @Route("/{page}", name="function_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_TEAM') or has_role('ROLE_DEMO_USER')")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction($page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('RichPeachTeamBundle:FunctionTable')
            ->getListQueryBuilder();

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            FunctionTable::FUNCTIONS_PER_PAGE
        );

        return $this->render('@RichPeachTeam/FunctionTable/index.html.twig', compact('pagination'));
    }

    /**
     * @Route("/new", name="function_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $functionTable = new FunctionTable();
        $form = $this->createForm(FunctionTableType::class, $functionTable);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($functionTable);
            $em->flush();

            return $this->redirect($this->generateUrl('function_index'));
        }

        return $this->render(
            '@RichPeachTeam/FunctionTable/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'new',
            ]
        );
    }

    /**
     * @Route("/edit/{id}", name="function_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("functionTable", class="RichPeachTeamBundle:FunctionTable")
     * @param Request $request
     * @param FunctionTable $functionTable
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, FunctionTable $functionTable)
    {
        $form = $this->createForm(FunctionTableType::class, $functionTable, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($functionTable);
            $em->flush();

            return $this->redirect($this->generateUrl('function_index'));
        }

        return $this->render(
            '@RichPeachTeam/FunctionTable/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'edit',
            ]
        );
    }

    /**
     * @Route("/delete/{id}", name="function_delete")
     * @Method("DELETE")
     * @ParamConverter("functionTable", class="RichPeachTeamBundle:FunctionTable")
     * @param Request $request
     * @param FunctionTable $functionTable
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, FunctionTable $functionTable)
    {
        $this->processEntityRemoval(
            $functionTable,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('function_index'));
    }

    /**
     * @Route("/get_functions_for_department", name="function_get_functions_for_department")
     * @Method({"POST", "GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \LogicException
     */
    public function getFunctionsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $departmentId = $request->query->get('value');
        $functionsDepartment = $em->getRepository('RichPeachTeamBundle:FunctionTable')
            ->findFunctionsByDepartment($departmentId);
        return new JsonResponse($functionsDepartment);
    }

}
