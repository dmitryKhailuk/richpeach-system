<?php
namespace RichPeach\TeamBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RichPeach\BookkeepingBundle\Entity\Currency;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="EmployeeRepository")
 * @ORM\Table(
 *     name="employees",
 *     schema="team",
 *     indexes={
 *          @ORM\Index(name="employees_name_surname_unique_idx", columns={"name", "surname"})
 *      }
 * )
 */
class Employee
{
    use TimestampableEntity;

    const EMPLOYEES_PER_PAGE = 25;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=35, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="35")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="surname", type="string", length=35, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="35")
     * @var string
     */
    private $surname;

    /**
     * @ORM\Column(name="patronymic", type="string", length=35, nullable=true)
     * @Assert\Length(max="35")
     * @var string
     */
    private $patronymic;

    /**
     * @ORM\Column(name="birthday", type="date", nullable=true)
     * @Assert\Date()
     * @var \DateTime
     */
    private $birthday;

    /**
     * @ORM\Column(name="passport", type="string", length=20, nullable=true)
     * @Assert\Length(max="20")
     * @var string
     */
    private $passport;

    /**
     * @ORM\Column(name="phone", type="string", length=15, nullable=true)
     * @Assert\Length(max="15")
     * @var string
     */
    private $phone;

    /**
     * @ORM\Column(name="address", type="string", length=150, nullable=true)
     * @var string
     */
    private $address;

    /**
     * @ORM\Column(name="salary", type="integer", nullable=true, options={"default"=0})
     * @var integer
     */
    private $salary = 0;

    /**
     * @ORM\Column(name="card_number", type="string", length=20, nullable=true)
     * @Assert\Length(max="20")
     * @var string
     */
    private $cardNumber;

    /**
     * @ORM\Column(name="date_employment", type="date", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Date()
     * @var \DateTime
     */
    private $dateEmployment;

    /**
     * @ORM\Column(name="date_dismissal", type="date", nullable=true)
     * @Assert\Date()
     * @var \DateTime
     */
    private $dateDismissal;

    /**
     * @ORM\OneToOne(
     *     targetEntity="Department",
     *     mappedBy="employee"
     * )
     * @var Department
     */
    private $department;

    /**
     * @ORM\ManyToOne(targetEntity="FunctionTable")
     * @ORM\JoinColumn(name="function_id", referencedColumnName="id", nullable=true)
     * @Assert\NotBlank()
     * @var FunctionTable
     */
    private $functionTable;

    /**
     * @ORM\ManyToOne(targetEntity="PaymentMethod")
     * @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id", nullable=true)
     * @Assert\NotBlank()
     * @var PaymentMethod
     */
    private $paymentMethod;

    /**
     * @ORM\Column(name="comment", type="text", nullable=true)
     * @var string
     */
    private $comment;

    /**
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id", nullable=true)
     * @ORM\ManyToOne(targetEntity="RichPeach\BookkeepingBundle\Entity\Currency",
     *     cascade={"persist"})
     * @var Currency
     */
    private $currency;

    public function __toString()
    {
        return $this->surname . ' ' . $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Employee
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Employee
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set patronymic
     *
     * @param string $patronymic
     *
     * @return Employee
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    /**
     * Get patronymic
     *
     * @return string
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return Employee
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set passport
     *
     * @param string $passport
     *
     * @return Employee
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;

        return $this;
    }

    /**
     * Get passport
     *
     * @return string
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Employee
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Employee
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set salary
     *
     * @param string $salary
     *
     * @return Employee
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get salary
     *
     * @return string
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * Set cardNumber
     *
     * @param string $cardNumber
     *
     * @return Employee
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * Get cardNumber
     *
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * Set dateEmployment
     *
     * @param \DateTime $dateEmployment
     *
     * @return Employee
     */
    public function setDateEmployment($dateEmployment)
    {
        $this->dateEmployment = $dateEmployment;

        return $this;
    }

    /**
     * Get dateEmployment
     *
     * @return \DateTime
     */
    public function getDateEmployment()
    {
        return $this->dateEmployment;
    }

    /**
     * Set dateDismissal
     *
     * @param \DateTime $dateDismissal
     *
     * @return Employee
     */
    public function setDateDismissal($dateDismissal)
    {
        $this->dateDismissal = $dateDismissal;

        return $this;
    }

    /**
     * Get dateDismissal
     *
     * @return \DateTime
     */
    public function getDateDismissal()
    {
        return $this->dateDismissal;
    }

    /**
     * Set department
     *
     * @param \RichPeach\TeamBundle\Entity\Department $department
     *
     * @return Employee
     */
    public function setDepartment(Department $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \RichPeach\TeamBundle\Entity\Department
     */
    public function getDepartment()
    {
        $department = $this->department;

        if ($this->getFunctionTable() && $this->getFunctionTable()->getDepartment()) {
            $department = $this->getFunctionTable()->getDepartment();
        }

        return $department;
    }

    /**
     * Set functionTable
     *
     * @param \RichPeach\TeamBundle\Entity\FunctionTable $functionTable
     *
     * @return Employee
     */
    public function setFunctionTable(FunctionTable $functionTable = null)
    {
        $this->functionTable = $functionTable;

        return $this;
    }

    /**
     * Get functionTable
     *
     * @return \RichPeach\TeamBundle\Entity\FunctionTable
     */
    public function getFunctionTable()
    {
        return $this->functionTable;
    }

    /**
     * Set paymentMethod
     *
     * @param \RichPeach\TeamBundle\Entity\PaymentMethod $paymentMethod
     *
     * @return Employee
     */
    public function setPaymentMethod(PaymentMethod $paymentMethod = null)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return \RichPeach\TeamBundle\Entity\PaymentMethod
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Get itLeader
     *
     * @return boolean
     */
    public function getItLeader()
    {
        return $this->itLeader;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Employee
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set currency
     *
     * @param \RichPeach\BookkeepingBundle\Entity\Currency $currency
     *
     * @return Employee
     */
    public function setCurrency(Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \RichPeach\BookkeepingBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}
