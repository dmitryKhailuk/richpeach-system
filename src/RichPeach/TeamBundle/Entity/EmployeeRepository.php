<?php

namespace RichPeach\TeamBundle\Entity;


use Doctrine\ORM\EntityRepository;

class EmployeeRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('e')
            ->select(['e', 'd', 'f'])
            ->leftJoin('e.department',  'd')
            ->leftJoin('e.functionTable', 'f')
            ->where('e.dateDismissal IS NULL OR e.dateDismissal >= :date ')
            ->setParameter('date', new \DateTime('now'))
            ->orderBy('e.surname', 'ASC');
    }

    /**
     * @return integer
     */
    public function countWorkedEmployee()
    {
        return $this->createQueryBuilder('e')
            ->select('COUNT(e.id)')
            ->where('e.dateDismissal IS NULL OR e.dateDismissal >= :date')
            ->setParameter('date', new \DateTime('now'))
            ->getQuery()->getSingleScalarResult();
    }

    /**
     * @return array
     */
    public function allSalaryEmployee()
    {
        return $this->getTotalSum()->getQuery()->getArrayResult();
    }

    /**
     * @param int $departmentId
     * @return array
     */
    public function getEmployeesDepartment($departmentId)
    {
        return $this->createQueryBuilder('e')
            ->select('e.name, e.surname, f.name AS functionName, e.salary, c.name AS currency, e.dateEmployment')
            ->leftJoin('e.currency', 'c')
            ->join('e.functionTable', 'f')
            ->join('f.department', 'd')
            ->where('e.dateDismissal IS NULL')
            ->andWhere('d.id = :id')
            ->setParameters([
                'id' => $departmentId
            ])
            ->orderBy('e.surname', 'ASC')
            ->getQuery()->getArrayResult();
    }

    /**
     * @return Employee[]
     */
    public function getArchiveEmployees()
    {
        return $this->createQueryBuilder('e')
            ->where('e.dateDismissal IS NOT NULL OR e.dateDismissal < :date')
            ->setParameter('date', new \DateTime('now'))
            ->orderBy('e.surname', 'ASC')
            ->getQuery()->getResult();
    }

    /**
     * @param integer $departmentId
     * @return array
     */
    public function getTotalSumByDepartment($departmentId)
    {
        return $this->getTotalSum()
            ->join('e.functionTable', 'f')
            ->join('f.department', 'd')
            ->andWhere('d.id = :id')
            ->setParameter('id', $departmentId)
            ->getQuery()->getArrayResult();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function getTotalSum()
    {
        return $this->createQueryBuilder('e')
            ->select('SUM(e.salary) as payment, c.name as currency, c.code')
            ->innerJoin('e.currency', 'c')
            ->where('e.dateDismissal IS NULL OR e.dateDismissal > :date')
            ->setParameter('date', new \DateTime('now'))
            ->groupBy('c.code', 'c.name')
            ->orderBy('c.code', 'ASC');
    }
}
