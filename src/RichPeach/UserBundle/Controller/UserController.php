<?php

namespace RichPeach\UserBundle\Controller;

use RichPeach\UserBundle\Entity\User;
use RichPeach\UserBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{

    /**
     * @Route("/users/{page}", name="users_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_USER')")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction($page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('RichPeachUserBundle:User')
            ->getListQueryBuilder();

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            User::USERS_PER_PAGE
        );

        return $this->render('@RichPeachUser/User/index.html.twig', compact('pagination'));
    }

    /**
     * @Route("/users/new", name="users_new")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_SHOW_MODULE_USER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {

        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();

        $form = $this->createForm(UserType::class, $user, [
            UserType::USER_ROLE_SUPER_ADMIN => $this->checkOrUserRoot(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $user->setEmail($user->getUsername() . '.@gmail.com');
            $user->setEnabled(true);
            $user->setPlainPassword($user->getPassword());

            $userManager->updateUser($user);

            $em->flush();

            return $this->redirect($this->generateUrl('users_index'));
        }

        return $this->render(
            '@RichPeachUser/User/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'new',
            ]
        );
    }

    /**
     * @Route("/users/edit/{id}", name="users_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("user", class="RichPeachUserBundle:User")
     * @Security("has_role('ROLE_SHOW_MODULE_USER')")
     * @param Request $request
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, User $user)
    {
        $form = $this->createForm(UserType::class, $user, [
            'method' => 'PUT',
            UserType::UPDATE_USER => true,
            UserType::USER_ROLE_SUPER_ADMIN => $this->checkOrUserRoot(),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($form->get('password')->getData()) {
                $user->setPlainPassword($form->get('password')->getData());
            }

            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('users_index'));
        }

        return $this->render(
            '@RichPeachUser/User/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'edit',
                'user' => $user,
            ]
        );
    }

    /**
     * @Route("/users/enabled/{id}", name="users_enabled")
     * @Method("GET")
     * @ParamConverter("user", class="RichPeachUserBundle:User")
     * @Security("has_role('ROLE_SHOW_MODULE_USER')")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $user->isEnabled() ? $user->setEnabled(false) : $user->setEnabled(true);

        $em->persist($user);
        $em->flush();

        return $this->redirect($this->generateUrl('users_index'));
    }


    /**
     * @return bool
     */
    private function checkOrUserRoot()
    {
        $roles = $this->getUser()->getRoles();
        $roles = array_flip($roles);
        return array_key_exists(User::ROLE_SUPER_ADMIN, $roles) ? true : false;
    }
}