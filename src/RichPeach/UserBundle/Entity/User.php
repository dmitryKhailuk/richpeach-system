<?php

namespace RichPeach\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use RichPeach\BookkeepingBundle\Entity\Partner;

/**
 * @ORM\Entity(repositoryClass="UserRepository")
 * @ORM\Table(
 *     name="user",
 *     schema="auth"
 * )
 */
class User extends BaseUser
{
    const USERS_PER_PAGE = 25;
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(
     *     targetEntity="RichPeach\BookkeepingBundle\Entity\Partner",
     *     mappedBy="user"
     * )
     * @var Partner
     */
    private $partner;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    public function __construct()
    {
        if (!$this->createdAt) {
            $this->createdAt = new \DateTime();
        }

        $this->updatedAt = new \DateTime();
        parent::__construct();
    }

    /**
     * Sets createdAt.
     *
     * @param  \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets updatedAt.
     *
     * @param  \DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Returns updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * создаем соль
     * @param null|string $salt
     * @return $this
     */
    public function setSalt($salt)
    {
        $rand = rand(10000, 99999);
        $this->salt = hash('sha256', $rand . $this->getUsername() . $salt);

        return $this;
    }

    /**
     * Set partner
     *
     * @param \RichPeach\BookkeepingBundle\Entity\Partner $partner
     *
     * @return User
     */
    public function setPartner(Partner $partner = null)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * Get partner
     *
     * @return \RichPeach\BookkeepingBundle\Entity\Partner
     */
    public function getPartner()
    {
        return $this->partner;
    }
}
