<?php

namespace RichPeach\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('u')
            ->orderBy('u.username', 'ASC')
            ->addOrderBy('u.enabled', 'ASC');
    }
}