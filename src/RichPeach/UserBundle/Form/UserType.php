<?php

namespace RichPeach\UserBundle\Form;

use RichPeach\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    const UPDATE_USER = 'user_update';
    const USER_ROLE_SUPER_ADMIN = 'user_role_super_admin';
    private $roles = [];

    /**
     * UserType constructor.
     * @param array $roles
     */
    public function __construct(array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options[self::UPDATE_USER] !== true) {
            $builder->add('username', TextType::class, [
                'label' => 'user.username',
                'required' => true,
            ]);
        }

        $builder->add('password', PasswordType::class, [
            'label' => 'user.password',
            'required' => false,
            'data' => null,
        ]);

        $builder->add('roles', ChoiceType::class, [
            'label' => 'user.roles',
            'required' => true,
            'expanded' => true,
            'multiple' => true,
            'choices' => $this->getRoles($options[self::USER_ROLE_SUPER_ADMIN]),
        ]);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();
            $disabled = array_key_exists('password', $data) ? false : true;

            $form->add('password', PasswordType::class, [
                'label' => 'user.password',
                'disabled' => $disabled,
                'data' => null,
            ]);

            $event->setData($data);
        });

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => User::class,
            self::UPDATE_USER => false,
            self::USER_ROLE_SUPER_ADMIN => false,

        ]);

        $resolver->setAllowedValues(self::UPDATE_USER, [true, false]);
        $resolver->setAllowedValues(self::USER_ROLE_SUPER_ADMIN, [true, false]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'richpeach_user_registration_type';
    }

    /**
     * @param bool $isRoot
     * @return array
     */
    private function getRoles($isRoot)
    {
        $roles = [];
        foreach ($this->roles  as $key => $role) {
            $roles[str_replace('_', ' ', $key)] = $key;
        }
        if (!$isRoot) {
            unset($roles[User::ROLE_SUPER_ADMIN]);
        }
        return $roles;
    }

}