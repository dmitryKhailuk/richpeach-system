<?php

namespace RichPeach\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class RichPeachUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
